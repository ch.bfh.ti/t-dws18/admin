# t-DWT Cockpit

* [Link Project Agreement](https://docs.google.com/document/d/17SKU3mIeNAbmcAax7EjafUGwwMaAtVZpG_XA5oc_2eY/edit?usp=sharing)
* [Link Project Specifications](https://docs.google.com/document/d/1xH2N1zdllP9B68IDpeLUldpAS11B0aT1XDqRcfDtILg/edit?usp=sharing)
* [Link Project Reporting](https://docs.google.com/document/d/1atyxKwkNeuh2ftl1CGWh-My3teGvWSj8JcIwZW1M2yk/edit?usp=sharing)
* [Link to Issue Board](https://gitlab.com/groups/ch.bfh.ti.t-dws18/-/boards) ([Milestone overview](https://gitlab.com/groups/ch.bfh.ti.t-dws18/-/milestones?sort=name_asc&state=all))
* [Link to SonarQube (use GitLab Login)](https://sonarqube.kocher.it) 



## Other important stuff
* [Android APK](https://gitlab.com/ch.bfh.ti.t-dws18/dws-manager/-/jobs/artifacts/master/raw/app/build/outputs/apk/debug/app-debug.apk?job=build)
* [BFH Docker Host](ssh://bti7251@ak.haemele.ch:22999) (Login in OneNote)
* [Portainer](https://portainer.haemele.ch) (Login in OneNote)
* [Traefik Dashboard](https://traefik.haemele.ch) (Login in OneNote)
* Mosquitto reachability:
  * host: mq.haemele.ch
  * TCP/1883, TCP/443 (secure websocks), TCP/8080 (websocks)
